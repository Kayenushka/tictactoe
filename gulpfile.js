var gulp = require('gulp');
var server = require('gulp-server-livereload');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('default', ['startserver','watch']);

gulp.task('startserver', function () {
    gulp.src('app').pipe(server({
        livereload: true,
        port: 7000,
        open: true
    }));
});

gulp.task('watch', function () {
    gulp.watch('app/css/dev/*.css', ['autostyle']);
});

gulp.task('autostyle', function () {
    return gulp.src('app/css/dev/*.css')
        .pipe(autoprefixer({
            browsers: ['last 15 versions']
        }))
        .pipe(gulp.dest('app/css'));
});