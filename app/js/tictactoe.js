'use strict';

var turn = 0;

document.getElementById('menu').classList.toggle("show");

function startgame() {
    document.getElementById('menu').classList.toggle("show");
    document.getElementById('game').classList.toggle("show");
}

document.getElementById('game').onclick = function (event) {

    if (event.target.className == 'block' && !event.target.innerHTML) {

        if (turn % 2 == 0) {
            event.target.innerHTML = 'O';
        } else {
            event.target.innerHTML = 'X';
        }
        checkWinner();

    }

}

function checkWinner() {

    var blocks = document.getElementsByClassName('block');
    var winner;

    for (var i = 0; i < 9; i += 3) {
        if (blocks[i].innerHTML && blocks[i].innerHTML == blocks[i + 1].innerHTML && blocks[i].innerHTML == blocks[i + 2].innerHTML) {
            winner = (turn % 2 == 0 ? 'O' : 'X');
        }
    };
    for (var i = 0; i < 3; i++) {
        if (blocks[i].innerHTML && blocks[i].innerHTML == blocks[i + 3].innerHTML && blocks[i].innerHTML == blocks[i + 6].innerHTML) {
            winner = (turn % 2 == 0 ? 'O' : 'X');
        }
    };

    if (blocks[0].innerHTML && blocks[0].innerHTML == blocks[4].innerHTML && blocks[0].innerHTML == blocks[8].innerHTML) {
        winner = (turn % 2 == 0 ? 'O' : 'X');
    };

    if (blocks[2].innerHTML && blocks[2].innerHTML == blocks[4].innerHTML && blocks[2].innerHTML == blocks[6].innerHTML) {
        winner = (turn % 2 == 0 ? 'O' : 'X');
    };

    if (!winner && turn == 9) {
        winner = "FRIENDSHIP";
    }

    if (winner) {
        //show winner
        document.getElementById('winner').innerHTML = winner + " WIN";
        document.getElementById("menu").classList.toggle("show");
        document.getElementById("game").classList.toggle("show");
        for (var i = 0; i < blocks.length; i++) {
            blocks[i].innerHTML = "";
        };
        turn = 0;
    }

    turn++;

}